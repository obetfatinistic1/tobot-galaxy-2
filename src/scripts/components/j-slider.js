import $ from 'jquery'
import 'slick-carousel'

const jSlider = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.type1 = $('[j-slider="type-1"]')
    this.type2 = $('[j-slider="type-2"]')
    this.type3 = $('[j-slider="type-3"]')
  },
  events: function() {
    this.onType3()
    this.onType1()
    this.onType2()
  },
  onType1: function() {
    this.type1.slick({
      autoplay: true,
      arrows: true,
      dots: true,
      prevArrow: '<img src="../../assets/img/ic-chevron-left-yellow.png" class="j-slider-arrow j-left">',
      nextArrow: '<img src="../../assets/img/ic-chevron-right-yellow.png" class="j-slider-arrow j-right">'
    })
  },
  onType2: function() {
    this.type2.slick({
      arrows: true,
      dots: true,
      appendDots: $('.j-hw-box-head'),
      prevArrow: '<div class="j-slider-arrow j-left j-half-circle"><img src="../../assets/img/ic-chevron-left-yellow.png"></div>',
      nextArrow: '<div class="j-slider-arrow j-right j-half-circle"><img src="../../assets/img/ic-chevron-right-yellow.png"></div>'
    })
  },
  onType3: function() {
    this.type3.slick({
      arrows: false,
      dots: true,
      appendDots: $('.j-hw-box-bulanan-dots')
    })
  }
}

export default jSlider
