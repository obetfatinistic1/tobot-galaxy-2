const jSquare = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.box = document.querySelectorAll('.j-square')
  },
  events: function() {
    this.onChange()
  },
  onChange: function() {
    this.box.forEach((e) => {
      const width = e.offsetWidth

      e.style.height = width + 'px'
    })
  }
}

export default jSquare
