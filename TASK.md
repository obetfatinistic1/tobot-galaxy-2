- Pengerjaan:
  - [x] Header | Responsive
  - [x] Footer | Responsive
  - [x] Syarat dan Ketentuan | Responsive
  - [x] Gallery | Responsive
  - [x] Pemenang | Responsive
  - [x] Homepage | Responsive



- Tercapai:
  - [o] CSS | Setup
  - [o] JS | Setup
  - [o] Layout | Setup9
  - [o] Fonts | Setup
  - [o] Header | New Component
  - [o] Footer | New Component
  - [o] Syarat dan Ketentuan | New Page
  - [o] Gallery | New Page
  - [o] Pemenang | New Page
  - [o] Effects | Box Modal for Show video or image
  - [o] Effects | Scroll To Top
  - [o] Homepage | New Page
  - [o] Effects | Robot Animation
