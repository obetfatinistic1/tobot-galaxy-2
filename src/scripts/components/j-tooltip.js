import fade from 'fade'

const jTooltip = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.tooltip = document.querySelectorAll('.j-tooltip-icon')
    this.tooltipDescr = document.querySelectorAll('.j-tooltip-descr')
    this.tooltipClose = document.querySelectorAll('.j-tooltip-descr-close')
  },
  events: function() {
    this.onChange()

    this.tooltip.forEach((e) => { e.addEventListener('click', this.onShow.bind(this)) })
    this.tooltipClose.forEach((e) => { e.addEventListener('click', this.onClose.bind(this)) })
  },
  onChange: function() {
    this.tooltipDescr.forEach((e) => {
      setTimeout(() => { fade.out(e, () => { e.style.display = 'none' }) }, 5000)
    })
  },
  onShow: function(e) {
    const descr = e.currentTarget.parentNode.querySelector('.j-tooltip-descr')

    descr.style.display = 'block'
    setTimeout(() => { fade.in(descr) }, 50)
  },
  onClose: function(e) {
    const target = e.currentTarget.parentNode
    fade.out(target, () => { target.style.display = 'none' })
  }
}

export default jTooltip
