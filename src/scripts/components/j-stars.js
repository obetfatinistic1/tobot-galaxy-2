import $ from 'jquery'

const jStars = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.body = $('body')
    this.galaxy = $('.galaxy')
    this.iterator = 0
  },
  events: function() {
    this.onBlink()
  },
  onBlink: function() {
    this.galaxy.css('min-height', this.body.height() + 'px')

    while (this.iterator <= 600){
      const xposition = Math.random()
      const yposition = Math.random()
      const star_type = Math.floor((Math.random() * 3) + 1)
      const position = {
        'x': this.galaxy.width() * xposition,
        'y': this.galaxy.height() * yposition,
      }
      
      $('<div class="star star-type' + star_type + '"></div>').appendTo(this.galaxy).css({
        'top': position.y,
        'left': position.x
      })
      
      this.iterator++
    }
  }
}

export default jStars
