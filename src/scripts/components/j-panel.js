import jSquare from './j-square'

const jPanel = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.btn = document.querySelectorAll('[j-direction="panel"]')
    this.place = document.querySelectorAll('.j-panel-body-item')
  },
  events: function() {
    this.btn.forEach((e) => { e.addEventListener('click', this.onChange.bind(this)) })
  },
  onChange: function(e) {
    const targetId = e.currentTarget.getAttribute('j-target-id')
    const target = e.currentTarget.getAttribute('j-target')

    this.place.forEach((e) => {
      if (e.getAttribute('j-target-id') === targetId) {
        if (e.getAttribute('j-place') !== target) e.classList.remove('active')
        else e.classList.add('active')
      }
    })

    this.btn.forEach((e) => {
      if (e.getAttribute('j-target-id') === targetId) {
        if (e.getAttribute('j-target') !== target) e.classList.remove('active')
        else e.classList.add('active')
      }
    })

    jSquare.run()
  }
}

export default jPanel
