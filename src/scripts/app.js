import jPanel from './components/j-panel'
import jSquare from './components/j-square'
import jTooltip from './components/j-tooltip'
import jToTop from './components/j-to-top'
import jShow from './components/j-show'
import jSlider from './components/j-slider'
import jStars from './components/j-stars'

document.addEventListener('DOMContentLoaded', function() {
  jPanel.run()
  jSquare.run()
  jTooltip.run()
  jToTop.run()
  jShow.run()
  jSlider.run()
  jStars.run()
})
