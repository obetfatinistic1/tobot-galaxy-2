import scrolltop from 'simple-scrolltop'

const jToTop = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.btn = document.querySelector('[j-direction="to-top"]')
  },
  events: function() {
    this.btn.addEventListener('click', this.onTop.bind(this))
  },
  onTop: function() { scrolltop(0) }
}

export default jToTop
