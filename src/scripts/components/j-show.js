import fade from 'fade'

const jShow = {
  run: function() {
    this.dom()
    this.events()
  },
  dom: function() {
    this.btnShow = document.querySelectorAll('[j-direction="modal-show"')
    this.btnClose = document.querySelectorAll('[j-direction="modal-show-close"')
  },
  events: function() {
    this.btnShow.forEach((e) => { e.addEventListener('click', this.onShow.bind(this)) })
    this.btnClose.forEach((e) => { e.addEventListener('click', this.onClose.bind(this)) })
  },
  onShow: function(e) {
    const ini = e.currentTarget
    const id = ini.getAttribute('j-target-id')
    const place = document.querySelector(`[j-place-id="${id}"]`)
    const placeDescr = place.querySelector('.j-popup-box-place')

    placeDescr.innerHTML = ini.innerHTML
    place.style.display = 'flex'
    setTimeout(() => { fade.in(place, 750) }, 50)
  },
  onClose: function(e) {
    const ini = e.currentTarget
    const id = ini.getAttribute('j-target-id')
    const place = document.querySelector(`[j-place-id="${id}"]`)

    fade.out(place, 750, () => { place.style.display = 'none' })
  }
}

export default jShow
